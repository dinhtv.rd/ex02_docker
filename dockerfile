FROM python:3

WORKDIR /usr/src/app

RUN pip install flask

COPY . .

CMD ["python3","./ex02.py"]
